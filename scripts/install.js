var _ = require('lodash'),
    fs = require('fs'),
    path = require('path'),
    glob = require('glob'),
    exec = require('child_process').exec;
var logger = require(path.join(__dirname, '../server/helpers/logger.js'))('SETUP');

// Generate /options.json if it doesn't exist yet
require(path.join(__dirname, '../server/helpers/generate-json.js'));

// Execute npm install for each extension
glob(path.join(__dirname, '../extensions/*/'), function(err, directories) {
    _.each(directories, function(directory) {
        logger.log('Running \'npm install --production\' in \'%s\'...', directory)
        exec('npm install --production', { cwd: directory }, function(error, stdout, stderr) {
            if ( stdout ) console.log('\nstdout:\n' + stdout);
            if ( stderr ) console.log('\nstderr:\n' + stderr);

            if ( error !== null ) console.error('\nexec error:\n' + error);
        });
    });
});