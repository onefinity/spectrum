// Make sure we log the START line first for easier debugging
var path = require('path');
var pkg = require(path.join(__dirname, 'package.json')),
    logger = require(path.join(__dirname, 'server/helpers/logger.js'))('START');
logger.log('Starting Spectrum v' + pkg.version);

// Start Spectrum
var _ = require('lodash'),
    app = require('app'),
    BrowserWindow = require('browser-window');
var server = require(path.join(__dirname, 'server/server.js'));

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
var mainWindow = null;

// Quit when all windows are closed.
app.on('window-all-closed', function() {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if ( process.platform !== 'darwin' ) app.quit();
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.on('ready', function() {
    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 1280,
        height: 800,
        resizable: true,
        title: _.capitalize(pkg.name) + ' v' + pkg.version
    });

    // and load the index.html of the app.
    mainWindow.setMenu(null);
    mainWindow.loadURL('file://' + __dirname + '/index.html');

    // Emitted when the window is closed.
    mainWindow.on('closed', function() {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    }).on('page-title-updated', function(e) {
        e.preventDefault();
    });
});