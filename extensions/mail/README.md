# Mail (IMAP) extension for [Spectrum](http://bitbucket.org/cascornelissen/spectrum)
Extends Spectrum to flash your lights when you recieve a new e-mail

## Options
Create an `options.json` file in the root directory of this extension which can hold the following information:

* __username__ `string`: Username for plain-text authentication
* __password__ `string`: Password for plain-text authentication
* __host__ `string`: [_localhost_] Hostname or IP address of the IMAP server
* __port__ `integer`: [_993_] Port number of the IMAP server
* __lights__ `array`: Array containg light `integer` identifiers
* __color__ `string`: [_dd4b39_] RGB color code (without leading `#`) indicitating the color of the flashing light

### GMail & 2FA
If you have two-factor authentication enabled for GMail (good on you!), you will have to generate an application password via [Google](https://security.google.com/settings/security/apppasswords) and use that in your options file.