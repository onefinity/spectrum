var path = require('path'),
    extend = require('extend'),
    request = require('request'),
    MailListener = require('mail-listener2');
var globalOptions = require(path.join(__dirname, '../../options.json'));

var options = extend(true, {}, {
    host: 'localhost',
    port: 993,
    color: 'dd4b39'
}, require(path.join(__dirname, 'options.json')));

var listener = new MailListener({
    host: options.host,
    username: options.username,
    password: options.password,
    port: options.port,
    tls: true,
    tlsOptions: {
        rejectUnauthorized: false
    },
    markSeen: false,
    fetchUnreadOnStart: false,
    attachments: false
});

// Start the mail listener
listener.start();

// Add other relevant event listeners to mail listener
listener.on('server:connected', function() {
    console.log('Connected to IMAP: %s - %s', options.host, options.username);
});
listener.on('server:disconnected', function() {
    console.log('Disconnected from IMAP: %s - %s', options.host, options.username);
});
listener.on('error', function(err) {
    console.error('The following error occurred in the Mail extension:\n', err);
});

// Listen for new mails
listener.on('mail', function() {
    console.log('New email received, flashing lights!');
    request('http://localhost:' + globalOptions.server.port + '/lights/4/notify/' + options.color);
});