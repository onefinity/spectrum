var fs = require('fs'),
    path = require('path');
var pkg = require(path.join(__dirname, '../package.json'));

var dest = path.join(__dirname, '../options.json');
var options = {
    host: '',
    username: '',
    password: '',
    lights: []
};

try {
    var stats = fs.lstatSync(dest);
    if ( stats.isFile() ) console.log('Successfully installed the Mail extension (v%s) for Spectrum!', pkg.version);
} catch(e) {
    fs.writeFile(dest, JSON.stringify(options, null, 4), function(err) {
        if ( err ) console.error(err);
        console.log('Successfully installed the Mail extension (v%s) for Spectrum!', pkg.version);
        console.log('An options file was generated at %s, make sure to add the necessary details in there for the extension to work, you can check the README.md for more information.', dest);
    });
}