var gulp        = require('gulp'),
    gutil       = require('gulp-util'),
    spawn       = require('child_process').spawn,
    extend      = require('extend'),
    browserSync = require('browser-sync').create();
var config      = require('../config').browserSync,
    production  = require('../config').production;

gulp.task('browser-sync', ['watch'], function() {
    function runElectronApp(path, env) {
        var electron = require('electron-prebuilt');
        var options = {
            env: extend({ NODE_ENV: 'development' }, env, process.env),
            stdio: 'inherit'
        };

        return spawn(electron, [path], options);
    }
    function getRootUrl(options) {
        var port = options.get('port');
        return 'http://localhost:' + port;
    }
    function getClientUrl(options) {
        var connectUtils = require('browser-sync/lib/connect-utils');
        var pathname = connectUtils.clientScript(options);
        return getRootUrl(options) + pathname;
    }

    if ( !production ) {
        gutil.log(gutil.colors.green.bold('Initializing browser-sync...'));

        // Defube socket domain
        if ( typeof config.socket === 'undefined' ) config.socket = {};
        config.socket.domain = getRootUrl;

        browserSync.init(config, function(err, bs) {
            if ( err ) throw err;

            runElectronApp('./', {
                BROWSER_SYNC_CLIENT_URL: getClientUrl(bs.options)
            });
        });
    }
});