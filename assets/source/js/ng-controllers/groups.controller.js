var _ = require('lodash'),
    diff = require('deep-diff').diff,
    async = require('async'),
    HueApi = require('node-hue-api').HueApi;

module.exports = function($scope) {
    $scope.title = 'Groups';
    $scope.description = 'Manage the groups of lights that are available from your hub.';

    // Retrieve groups from Hub
    var api = new HueApi(localStorage.ip, localStorage.username);
    api.groups().then(function(groups) {
        async.parallel(_.map(groups, function(group) {
            return function(callback) {
                api.getGroup(group.id).then(function(status) { callback(null, status); }).done();
            };
        }), function(err, groups) {
            _.map(groups, function(group) {
                if ( group.id === '0' ) group.name = 'All lights';
                return group;
            });

            // Update scope with basic data about groups, light-related details will be added later
            $scope.$apply(function() {
                $scope.groups = groups;
            });

            // Get lights that are in a group to check if they're on or not
            var lights = _.union(_.flatten(_.map(groups, 'lights')));
            async.parallel(_.map(lights, function(light) {
                return function(callback) {
                    api.lightStatus(light).then(function(state) { callback(null, state); }).done();
                };
            }), function(err, states) {
                $scope.$apply(function() {
                    $scope.groups = _.map(groups, function(group) {
                        group.lights = _.map(group.lights, function(light) {
                            return {
                                id: light,
                                on: states[_.findIndex(lights, function(l) { return l === light; })].state.on
                            };
                        });

                        // Check wheter 'all' (true), 'some', or 'none' (false) lights are on
                        group.on = ( _.all(group.lights, 'on', false) ) ? false : true;
                        group.state = ( _.all(group.lights, 'on') ) ? 'all' : ( _.some(group.lights, 'on') ) ? 'some' : 'none';

                        return group;
                    });
                });

                $scope.$watch('groups', function(newValue, oldValue) {
                    // Check which group was changed
                    var differences = diff(newValue, oldValue);
                    if ( typeof differences === 'undefined' ) return;

                    var supported_keys = {
                        'on': 'on'
                    };

                    _.each(differences, function(change) {
                        var id    = change.path[0],
                            key   = _.drop(change.path).join('.'),
                            group = newValue[id].id,
                            value = change.lhs;

                        if ( change.kind === 'E' && _.contains(_.keys(supported_keys), key)) {
                            // Generate LightState object
                            var state = {};
                            state[supported_keys[key]] = value;

                            // Update indeterminate class
                            console.log(id);
                            $scope.groups[id].state = ( state.on ) ? 'all' : 'none';

                            // Update light
                            api.setGroupLightState(group, state).fail(function(err) {
                                console.error(err);
                            }).done();
                        }
                    });
                }, true);
            });
        });
    }).done();

    // Opening/closing detailed information containers
    $scope.open = function(group) {
        group.open = !group.open;
    };
};