module.exports = function($scope, $state, Config, bridgeFactory, extensionsFactory) {
    $scope.config = Config;
    $scope.loading = true;

    // Make sure we're actually connected to the bridge
    bridgeFactory.check().then(function() {
        $scope.loading = false;
    }, function() {
        $state.go('setup.automatic'); // TODO: Create another route with an explanation of what happened (couldn't reconnect)
    });

    extensionsFactory.startAll().then(function() {

    });
};