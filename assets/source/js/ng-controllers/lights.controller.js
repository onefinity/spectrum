var _ = require('lodash'),
    diff = require('deep-diff').diff,
    async = require('async'),
    HueApi = require('node-hue-api').HueApi;

module.exports = function($scope, $timeout) {
    $scope.title = 'Lights';
    $scope.description = 'Control all available lights; turn them on or off, change their color, and update their properties.';

    // Retrieve lights from Hub
    var api = new HueApi(localStorage.ip, localStorage.username);
    $scope.update = function() {
        api.lights().then(function(lights) {
            lights = lights.lights;

            async.parallel(_.map(lights, function(light) {
                return function(callback) {
                    api.lightStatus(light.id).then(function(state) { callback(null, state); }).done();
                };
            }), function(err, states) {
                // Update $scope
                $scope.$apply(function() {
                    $scope.lights = _.map(lights, function(light, i) {
                        return _.extend({}, lights[i], states[i], {
                            open: false,
                            slider: {
                                hue: states[i].state.hue,
                                sat: states[i].state.sat,
                                bri: states[i].state.bri
                            }
                        });
                    });
                });

                // Watch for light-related updates
                $scope.$watch('lights', function(newValue, oldValue) {
                    // Check which light was changed
                    var differences = diff(newValue, oldValue);
                    if ( typeof differences === 'undefined' ) return;

                    var supported_keys = {
                        'state.on': 'on',
                        'state.hue': 'hue',
                        'state.sat': 'sat',
                        'state.bri': 'bri'
                    };

                    // Iterate over differences and update lights accordingly
                    _.each(differences, function(change) {
                        var id    = change.path[0],
                            key   = _.drop(change.path).join('.'),
                            light = newValue[id].id,
                            value = change.lhs;

                        if ( change.kind === 'E' && _.contains(_.keys(supported_keys), key)) {
                            // Generate LightState object
                            var state = {};
                            state[supported_keys[key]] = value;

                            // Make sure to turn it on when it's not on already
                            if ( typeof state.on === 'undefined' ) {
                                state.on = true;
                                $scope.lights[id].state.on = true;
                            }

                            // Update light
                            api.setLightState(light, state).fail(function(err) {
                                console.error(err);
                            }).done();
                        }
                    });
                }, true);
            });
        }).done();
    }();

    // Opening/closing detailed information containers
    $scope.open = function(light) {
        light.open = !light.open;

        // Update sliders
        $timeout(function() {
            $scope.$broadcast('rzSliderForceRender');
        });
    };
};