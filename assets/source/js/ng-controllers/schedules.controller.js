module.exports = function($scope) {
    $scope.title = 'Schedules';
    $scope.description = 'Control your recurring alarms and schedules.';
};