module.exports = function($scope) {
    $scope.title = 'API builder';
    $scope.description = 'Use this tool to build URLs that can be used by developers in their own applications to control the lights.';
};