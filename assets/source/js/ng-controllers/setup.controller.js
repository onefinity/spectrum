module.exports = function($scope, $rootScope, $timeout, $interval, $state, $http, Config, bridgeFactory) {
    $scope.connectInterval = null;

    // Setup
    $scope.setup = function() {
        $scope.status = 'searching';

        // First, let's find a Philips Hue Bridge
        bridgeFactory.find().then(function(ip) {
            // Connect to the one we found
            bridgeFactory.connect(ip).then(function(result) {
                // If a username was returned that would mean a user with that username is registered and that we can connect successfully
                // If not, we'll have to register a new user and continue from there
                var username = result.username;
                if ( username ) {
                    if ( Config.debug ) console.debug('User \'%s\' is registered... Let\'s go!', username);

                    // Update username in /options.json
                    $http.get('http://localhost:384/username/' + username).then(function() {
                        $state.go('dashboard.lights');
                    }, function(err) {
                        if ( err.status !== 404 ) {
                            console.error(err);
                            return;
                        }

                        $state.go('dashboard.lights');
                    });
                } else {
                    $timeout(function() {
                        if ( Config.debug ) console.debug('User is not registered yet, registering now...');

                        $scope.connectInterval = $interval(function() {
                            $scope.status = 'user-setup';

                            bridgeFactory.register(ip).then(function(username) {
                                if ( Config.debug ) console.debug('Successfully registered user with username \'%s\'', username);

                                $interval.cancel($scope.connectInterval);

                                // Update username in /options.json
                                // TODO: Show a welcome message
                                $http.get('http://localhost:384/username/' + username).then(function() {
                                    $state.go('dashboard.lights');
                                }, function(err) {
                                    if ( err.status !== 404 ) {
                                        console.error(err);
                                        return;
                                    }

                                    $state.go('dashboard.lights');
                                });
                            }, function(reason) {
                                if ( Config.debug ) console.debug('Could not register user (%s), trying again...', reason, localStorage.ip);
                            });
                        }, 15 * 1000);
                    }, 2500 + (Math.random() * 1500)); // Make sure users appreciate the effort put into executing this action
                }
            }, function(reason) {
                $timeout(function() {
                    if ( Config.debug ) console.debug('Could not connect to bridge...', reason);
                    $scope.status = 'not-found';
                }, 2500 + (Math.random() * 1500)); // Make sure users appreciate the effort put into executing this action
            });
        }, function(reason) {
            $timeout(function() {
                if ( Config.debug ) console.debug('Could not find bridge...', reason);
                $scope.status = 'not-found';
            }, 2500 + (Math.random() * 1500)); // Make sure users appreciate the effort put into executing this action
        });
    };

    if ( $state.current.name === 'setup.automatic' ) $scope.setup();

    $rootScope.$on('$stateChangeSuccess', function(event, toState) {
        if ( $scope.connectInterval ) $interval.cancel($scope.connectInterval);
        if ( toState.name === 'setup.automatic' ) $scope.setup();
    });
};