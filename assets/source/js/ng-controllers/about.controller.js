module.exports = function($scope, Config) {
    $scope.title = 'About';
    $scope.description = 'Get detailed information about your Hue system and the Spectrum application.';
    $scope.config = Config;
};