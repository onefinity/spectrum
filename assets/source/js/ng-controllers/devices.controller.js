var _ = require('lodash'),
    HueApi = require('node-hue-api').HueApi;

module.exports = function($scope) {
    $scope.title = 'Devices';
    $scope.description = 'Manage registered devices and review details about each single registered device.';
    $scope.devices = [];

    var api = new HueApi(localStorage.ip, localStorage.username);

    // Retrieve lights from Hub
    $scope.update = function() {
        api.registeredUsers().then(function(devices) {
            $scope.$apply(function() {
                $scope.devices = devices.devices.map(function(device) {
                    if ( localStorage.username === device.username ) {
                        device.name += ' (this instance)';
                        device.disabled = true;
                    }

                    return device;
                });
            });
        }).done();
    }();

    // Deleting existing devices
    $scope.delete = function(username) {
        var index = _.findIndex($scope.devices, function(device) {
            return device.username === username;
        });
        var device = $scope.devices[index];

        device.disabled = true;
        if ( confirm('Are you sure you want to delete \"' + device.name + '\" (' + device.username + ') from the whitelisted devices?') ) {
            api.deleteUser(device.username).then(function() {
                $scope.$apply(function() {
                    $scope.devices.splice(index, 1);
                });
            }).fail(function(err) {
                device.disabled = false;

                console.error(err);
                alert('Could not delete \"' + device.name + '\"');
            }).done();
        } else {
            device.disabled = false;
        }
    };
};