module.exports = function($scope) {
    $scope.title = 'Triggers';
    $scope.description = 'Let different services connect to Spectrum and trigger lights by using this simple API.';
};