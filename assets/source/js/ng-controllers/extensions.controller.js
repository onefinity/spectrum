module.exports = function($scope, extensionsFactory) {
    $scope.title = 'Extensions';
    $scope.description = 'Expand the functionalities of Spectrum by adding extensions.';

    extensionsFactory.get().then(function(extensions) {
        $scope.extensions = extensions;
    });
};