var _ = require('lodash');

module.exports = function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'ng-directives/colorpicker.html',
        controller: ['$scope', function($scope) {
            $scope.color = 'hsl(' + _.remap($scope.light.slider.hue, 0, 65535, 0, 360) + ', 75%, 50%)';
            $scope.options = {
                hue: {
                    floor: 0,
                    ceil: 65535,
                    onChange: function() {
                        // Adjust other sliders
                        $scope.color = 'hsl(' + _.remap($scope.light.slider.hue, 0, 65535, 0, 360) + ', 75%, 50%)';
                    },
                    onEnd: function() {
                        // Only update the actual light when the user stops dragging
                        $scope.light.state.hue = $scope.light.slider.hue;
                    }
                },
                sat: {
                    floor: 0,
                    ceil: 255,
                    onChange: function() {
                        // Adjust other sliders
                    },
                    onEnd: function() {
                        // Only update the actual light when the user stops dragging
                        $scope.light.state.sat = $scope.light.slider.sat;
                    }
                },
                bri: {
                    floor: 0,
                    ceil: 255,
                    onChange: function() {
                        // Adjust other sliders
                    },
                    onEnd: function() {
                        // Only update the actual light when the user stops dragging
                        $scope.light.state.bri = $scope.light.slider.bri;
                    }
                }
            };
        }]
    };
};