var _ = require('lodash'),
    $ = require('jquery'),
    async = require('async'),
    urljoin = require('url-join');

module.exports = function($q, $http, $interval, Config) {
    window._extensions = [];

    return {
        get: function() {
            var d = $q.defer();

            $http.get(urljoin(Config.api.url, 'extensions')).then(function(response) {
                d.resolve(response.data);
            }, function(response) {
                d.reject(response);
            });

            return d.promise;
        },
        start: function(extension) {
            var d = $q.defer();
            var start = function(extension) {
                if ( Config.debug ) console.debug('Loading extension: %s v%s', extension.name, extension.version);

                // Generate full-blown URL
                var a = document.createElement('a');
                a.href = extension.url;

                // Load and execute the extension
                $.getScript(a.href, function() {
                    if ( _.isNumber(_extensions[extension.id].interval) ) {
                        var interval = _extensions[extension.id].interval;
                        if ( Config.debug ) console.debug('Starting extension: %s v%s at an interval of %ims', extension.name, extension.version, interval);

                        // Immediately start the extension and start running the main function at the specified interval
                        _extensions[extension.id].fn();
                        $interval(_extensions[extension.id].fn, interval);
                    } else {
                        if ( Config.debug ) console.debug('Starting extension: %s v%s', extension.name, extension.version);

                        // Start the extension
                        _extensions[extension.id].fn();
                    }
                });
            };

            // If an extension object was provided just start the extension
            // If an extension id (string) was provided, get the data about that specific extension and then start the extension
            if ( _.isPlainObject(extension) ) {
                start(extension);
            } else if ( _.isString(extension) ) {
                var id = extension;
                this.get().then(function(extensions) {
                    start(_.find(extensions, function(extension) {
                        return extension.id === id;
                    }));
                });
            }

            return d.promise;
        },
        startAll: function() {
            var d = $q.defer();

            var self = this;
            this.get().then(function(extensions) {
                async.parallel(_.map(extensions, function(extension) {
                    return function(callback) {
                        if ( extension.client ) self.start(extension);
                        callback();
                    };
                }), function(err) {
                    if ( err ) {
                        d.reject();
                        return false;
                    }

                    d.resolve();
                });
            });

            return d.promise;
        }
    };
};