var hue = require('node-hue-api'),
    extend = require('extend');

module.exports = function($q, Config) {
    return {
        find: function() {
            var d = $q.defer();

            hue.nupnpSearch().then(function(bridges) {
                if ( bridges.length ) {
                    localStorage.ip = bridges[0].ipaddress;
                    d.resolve(bridges[0].ipaddress);
                } else {
                    d.reject('No bridges were found');
                }
            }).fail(function() {
                d.reject();
            }).done();

            return d.promise;
        },
        connect: function(ip, username) {
            var d = $q.defer();

            if ( !ip ) {
                d.reject('IP was not provided');
            } else {
                if ( typeof username === 'undefined' ) {
                    if ( localStorage.username ) {
                        if ( Config.debug ) console.debug('Username \'%s\' is stored in localStorage, let\'s see if it\'s valid...', localStorage.username);
                        username = localStorage.username;
                    } else {
                        d.resolve(false);
                    }
                }

                if ( username ) {
                    var api = new hue.HueApi(ip, username);

                    api.config().then(function(result) {
                        if ( typeof result.linkbutton === 'undefined' ) {
                            d.resolve(false);
                        } else {
                            d.resolve({
                                username: username,
                                config: result
                            });
                        }
                    }).fail(function() {
                        d.reject('Checking if user \'' + username + '\' exists on hub with IP \'' + ip + '\' failed.');
                    }).done();
                }
            }

            return d.promise;
        },
        register: function(ip) {
            var d = $q.defer();

            if ( !ip ) {
                d.reject('IP was not provided');
            } else {
                var api = new hue.HueApi();

                api.registerUser(ip, null, 'Spectrum').then(function(result) {
                    localStorage.username = result;
                    d.resolve(result);
                }).fail(function() {
                    d.reject('Link button has not been pressed yet...');
                }).done();
            }

            return d.promise;
        },
        check: function() {
            var d = $q.defer(),
                ip = localStorage.ip,
                username = localStorage.username;

            // Checks if the bridge is still up at localStorage.ip and localStorage.username can still connect
            if ( !ip || !username ) {
                d.reject('Could not check bridge: ip or username is not set (ip: ' + ip + ', username: ' + username + ')');
            } else {
                var api = new hue.HueApi(ip, username);

                api.config().then(function(result) {
                    if ( typeof result.ipaddress !== 'undefined' ) {
                        extend(Config.bridge, result);
                        d.resolve();
                    } else {
                        d.reject('Invalid username \'' + username + '\'');
                    }
                }).fail(function(err) {
                    console.error('err', err);
                    d.reject('Could not connect to IP \'' + ip + '\'');
                }).done();
            }

            return d.promise;
        }
    };
};