var _ = require('lodash'),
    $ = require('jquery'),
    angular = require('angular');

// Lodash mixins
_.mixin({
    remap: function(value, start1, stop1, start2, stop2) {
        return start2 + (stop2 - start2) * ((value - start1) / (stop1 - start1));
    }
});

// Require Angular libraries
require('moment');
require('angular-ui-router');
require('angular-moment');
require('angularjs-slider');

// Initialize application
var app = angular.module('spectrumApp', ['ui.router', 'angularMoment', 'rzModule']);

// Directives
app.directive('colorpicker', require('./ng-directives/colorpicker.directive.js'));

// Controllers
app.controller('setupController', ['$scope', '$rootScope', '$timeout', '$interval', '$state', '$http', 'Config', 'bridgeFactory', require('./ng-controllers/setup.controller.js')]);
app.controller('dashboardController', ['$scope', '$state', 'Config', 'bridgeFactory', 'extensionsFactory', require('./ng-controllers/dashboard.controller.js')]);
app.controller('lightsController', ['$scope', '$timeout', require('./ng-controllers/lights.controller.js')]);
app.controller('groupsController', ['$scope', require('./ng-controllers/groups.controller.js')]);
app.controller('schedulesController', ['$scope', require('./ng-controllers/schedules.controller.js')]);
app.controller('triggersController', ['$scope', require('./ng-controllers/triggers.controller.js')]);
app.controller('extensionsController', ['$scope', 'extensionsFactory', require('./ng-controllers/extensions.controller.js')]);
app.controller('apibuilderController', ['$scope', require('./ng-controllers/api-builder.controller.js')]);
app.controller('devicesController', ['$scope', require('./ng-controllers/devices.controller.js')]);
app.controller('aboutController', ['$scope', 'Config', require('./ng-controllers/about.controller.js')]);

// Factories
app.factory('bridgeFactory', ['$q', 'Config', require('./ng-factories/bridge.factory.js')]);
app.factory('extensionsFactory', ['$q', '$http', '$interval', 'Config', require('./ng-factories/extensions.factory.js')]);

//  Config
app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('setup', {
            url: '/setup',
            templateUrl: 'ng-templates/setup.html',
            abstract: true
        })
        .state('setup.automatic', {
            url: '/automatic',
            templateUrl: 'ng-templates/setup/automatic.html'
        })
        .state('setup.manual', {
            url: '/manual',
            templateUrl: 'ng-templates/setup/manual.html'
        })

        .state('dashboard', {
            url: '/dashboard',
            templateUrl: 'ng-templates/dashboard.html',
            abstract: true
        })
        .state('dashboard.lights', {
            url: '/lights',
            templateUrl: 'ng-templates/dashboard/lights.html'
        })
        .state('dashboard.groups', {
            url: '/groups',
            templateUrl: 'ng-templates/dashboard/groups.html'
        })
        .state('dashboard.schedules', {
            url: '/schedules',
            templateUrl: 'ng-templates/dashboard/schedules.html'
        })

        .state('dashboard.extensions', {
            url: '/extensions',
            templateUrl: 'ng-templates/dashboard/extensions.html'
        })
        .state('dashboard.apibuilder', {
            url: '/api-builder',
            templateUrl: 'ng-templates/dashboard/api-builder.html'
        })
        .state('dashboard.devices', {
            url: '/devices',
            templateUrl: 'ng-templates/dashboard/devices.html'
        })
        .state('dashboard.about', {
            url: '/about',
            templateUrl: 'ng-templates/dashboard/about.html'
        })

        // Options
        .state('options', {
            url: '/options',
            templateUrl: 'ng-templates/options.html'
        });


    // Otherwise
    $urlRouterProvider.otherwise(function() {
        return '/setup';
    });

    // Redirects
    $urlRouterProvider.when('/dashboard', '/dashboard/lights');
    $urlRouterProvider.when('/setup', '/setup/automatic');
}]);

// Loading classes
app.run(['$rootScope', function($rootScope) {
    $rootScope.$on('$stateChangeStart', function() {
        $('body').addClass('loading');
    });

    $rootScope.$on('$stateChangeSuccess', function() {
        setTimeout(function() {
            $('body').removeClass('loading');
        }, 250);
    });
}]);

// Configuration
app.value('Config', (function() {
    // Get package.json
    var semver = require('semver'),
        pkg = require('../../../package.json');

    return {
        debug: true,
        version: {
            major: semver.major(pkg.version).toString(),
            minor: (semver.major(pkg.version) + '.' + semver.minor(pkg.version)).toString(),
            patch: pkg.version.toString()
        },
        author: pkg.author,
        repository: pkg.repository.url,
        api: {
            url: 'http://localhost:384'
        },
        bridge: {}
    };
})());