var _ = require('lodash'),
    hue = require('node-hue-api'),
    glob = require('glob'),
    path = require('path'),
    async = require('async'),
    hexrgb = require('hexrgb'),
    restify = require('restify');
var logger = require(path.join(__dirname, 'logger.js'))('API'),
    lights = require(path.join(__dirname, 'lights.js')),
    extensions = require(path.join(__dirname, 'extensions.js')),
    pkg = require(path.join(__dirname, '../../package.json')),
    options = require(path.join(__dirname, '../../options.json'));

module.exports = {
    create: function(api) {
        logger.log('Setting up API at \'http://localhost:%s\'...', options.server.port);

        // Server
        var server = restify.createServer({
            name: 'Spectrum',
            version: pkg.version
        });
        server.use(restify.CORS());
        server.pre(restify.pre.sanitizePath());

        //  - - Extensions - - //
        server.get('/extensions', function(req, res, next) {
            extensions.get().then(function(extensions) {
                res.send(extensions);
                return next();
            });
        });

        //  - - Lights - - //
        server.get('/lights', function(req, res) {
            api.lights().then(function(lights) {
                res.send(lights.lights);
                return next();
            }).done();
        });

        // Turns lights on
        server.get('/lights/:ids/on', function(req, res, next) {
            lights.update(api, lights.parseIDs(req.params.ids), hue.lightState.create().on());

            res.send(200);
            return next();
        });

        // Turns lights off
        server.get('/lights/:ids/off', function(req, res, next) {
            lights.update(api, lights.parseIDs(req.params.ids), hue.lightState.create().off());

            res.send(200);
            return next();
        });

        // Turns lights that are on off and the ones that are off on
        server.get('/lights/:ids/toggle', function(req, res, next) {
            var ids = lights.parseIDs(req.params.ids);

            var functions = {};
            _.each(ids, function(id) {
                functions[id] = function(callback) {
                    api.lightStatus(id).then(function(result) {
                        callback(null, result.state.on);
                    }).done();
                };
            });

            async.parallel(functions, function(err, results) {
                if ( err ) {
                    logger.error(err);

                    res.send(500);
                    return next();
                }

                var on = [],
                    off = [];

                _.each(results, function(result, light) {
                    if ( result === true ) on.push(light);
                    if ( result === false ) off.push(light);
                });

                lights.update(api, lights.parseIDs(on.join(',')), hue.lightState.create().off());
                lights.update(api, lights.parseIDs(off.join(',')), hue.lightState.create().on());

                res.send(200);
                return next();
            });
        });

        // Flash lights
        server.get('/lights/:ids/flash', function(req, res, next) {
            var ids = lights.parseIDs(req.params.ids);

            var functions = {};
            _.each(ids, function(id) {
                functions[id] = function(callback) {
                    api.lightStatus(id).then(function(result) {
                        callback(null, result);
                    }).done();
                };
            });

            async.parallel(functions, function(err, results) {
                if ( err ) {
                    logger.error(err);

                    res.send(500);
                    return next();
                }

                _.each(results, function(result, light) {
                    async.series([
                        function(callback) {
                            lights.update(api, light, hue.lightState.create().transition(0).off());
                            setTimeout(callback, 300);
                        },
                        function(callback) {
                            lights.update(api, light, hue.lightState.create().transition(0).on());
                            setTimeout(callback, 300);
                        },
                        function(callback) {
                            lights.update(api, light, hue.lightState.create().transition(0).off());
                            setTimeout(callback, 300);
                        },
                        function(callback) {
                            lights.update(api, light, hue.lightState.create().transition(0).on().hsb(result.state.hue, result.state.sat, result.state.bri).xy(result.state.xy[0], result.state.xy[1]));
                            callback();
                        }
                    ], function(err) {
                        if ( err ) {
                            logger.error(err);

                            res.send(500);
                            return next();
                        }

                        res.send(200);
                        return next();
                    });
                });
            });

            res.send(200);
            return next();
        });

        // Flashing in a specific color
        server.get('/lights/:ids/flash/:color', function(req, res, next) {
            var ids = lights.parseIDs(req.params.ids),
                color = hexrgb.hex2rgb('#' + req.params.color, true);

            var functions = {};
            _.each(ids, function(id) {
                functions[id] = function(callback) {
                    api.lightStatus(id).then(function(result) {
                        callback(null, result);
                    }).done();
                };
            });

            async.parallel(functions, function(err, results) {
                if ( err ) {
                    logger.error(err);

                    res.send(500);
                    return next();
                }

                _.each(results, function(result, light) {
                    async.series([
                        function(callback) {
                            lights.update(api, light, hue.lightState.create().transition(0).off());
                            setTimeout(callback, 300);
                        },
                        function(callback) {
                            lights.update(api, light, hue.lightState.create().transition(0).on().rgb(color));
                            setTimeout(callback, 300);
                        },
                        function(callback) {
                            lights.update(api, light, hue.lightState.create().transition(0).off());
                            setTimeout(callback, 300);
                        },
                        function(callback) {
                            lights.update(api, light, hue.lightState.create().transition(0).on().hsb(result.state.hue, result.state.sat, result.state.bri).xy(result.state.xy[0], result.state.xy[1]));
                            callback();
                        }
                    ], function(err) {
                        if ( err ) {
                            logger.error(err);

                            res.send(500);
                            return next();
                        }

                        res.send(200);
                        return next();
                    });
                });
            });

            res.send(200);
            return next();
        });

        // Notify in a specific color
        server.get('/lights/:ids/notify/:color', function(req, res, next) {
            var ids = lights.parseIDs(req.params.ids),
                color = hexrgb.hex2rgb('#' + req.params.color, true);

            var functions = {};
            _.each(ids, function(id) {
                functions[id] = function(callback) {
                    api.lightStatus(id).then(function(result) {
                        callback(null, result);
                    }).done();
                };
            });

            async.parallel(functions, function(err, results) {
                if ( err ) {
                    logger.error(err);

                    res.send(500);
                    return next();
                }

                _.each(results, function(result, light) {
                    async.series([
                        function(callback) {
                            lights.update(api, light, hue.lightState.create().transition(0).on().rgb(color));
                            setTimeout(callback, 350);
                        },
                        function(callback) {
                            lights.update(api, light, hue.lightState.create().transition(0).on().hsb(result.state.hue, result.state.sat, result.state.bri).xy(result.state.xy[0], result.state.xy[1]));
                            callback();
                        }
                    ], function(err) {
                        if ( err ) {
                            logger.error(err);

                            res.send(500);
                            return next();
                        }

                        res.send(200);
                        return next();
                    });
                });
            });

            res.send(200);
            return next();
        });

        server.listen(options.server.port);
    }
};