var _ = require('lodash');

module.exports = {
    parseIDs: function(ids) {
        return ids.split(',').map(function(v) {
            return parseInt(v, 10);
        });
    },
    update: function(api, lights, state) {
        _.each(lights, function(light) {
            api.setLightState(light, state).done();
        });
    }
};