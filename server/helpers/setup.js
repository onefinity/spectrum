var fs = require('fs'),
    path = require('path'),
    restify = require('restify'),
    detectIndent = require('detect-indent');
var logger = require(path.join(__dirname, 'logger.js'))('SETUP'),
    pkg = require(path.join(__dirname, '../../package.json'));

module.exports = function() {
    var optionsFile = path.join(__dirname, '../../options.json'),
        options = require(optionsFile);

    logger.log('No username has been been provided in Spectrum\'s global options file (%s), setting up a basic server to handle a username request from the client...', optionsFile);

    var server = restify.createServer({
        name: 'Spectrum setup',
        version: pkg.version
    });
    server.use(restify.CORS());
    server.pre(restify.pre.sanitizePath());

    server.get('/username/:username', function(req, res) {
        logger.log('POST request was made to /username with username \'%s\'', req.params.username);

        // Check if username was provided
        // TODO: Check validity of username
        if ( !req.params.username ) {
            res.send(400);
            return;
        }

        // Finish the request
        res.send(200);

        // Stop this server once all connections close (this can take some time)
        server.close(function() {
            logger.log('Closed Spectrum setup server');
        });

        // Make absolutely sure that the global options file exists and update global options file with the provided username
        require(path.join(__dirname, 'generate-json.js'));
        options.username = req.params.username;
        fs.writeFileSync(optionsFile, JSON.stringify(options, null, detectIndent(fs.readFileSync(optionsFile, 'utf8')).indent || '    '));

        // Start the normal server
        require(path.join(__dirname, 'start.js'));
    });
    server.get('/test', function(req, res) {
        res.send('still here');
        console.log('test');
    });

    server.listen(options.server.port);
}();