var _ = require('lodash'),
    Q = require('q'),
    fs = require('fs'),
    path = require('path'),
    glob = require('glob'),
    fork = require('child_process').fork,
    urljoin = require('url-join');
var logger = require(path.join(__dirname, 'logger.js'))('EXT');

module.exports = {
    start: function() {
        this.get().then(function(extensions) {
            _.each(extensions, function(extension) {
                if ( !extension.server ) return;

                logger.log('Starting server-side extension: %s v%s', extension.name, extension.version)
                fork(extension.server);
            })
        });
    },
    get: function() {
        var d = Q.defer();

        glob(path.join(__dirname, '../../extensions/*/'), function(err, extensions) {
            if ( err ) {
                logger.error(err);
                d.reject(err);
            }

            d.resolve(extensions.map(function(value) {
                var id = path.basename(value),
                    epkg = require(path.join(value, 'extension.json'));

                return {
                    id: id,
                    name: epkg.name,
                    author: epkg.author,
                    version: epkg.version,
                    path: path.normalize(value),
                    client: function() {
                        try {
                            var stats = fs.lstatSync(path.join(value, 'extension.js'));
                            return ( stats.isFile() ) ? urljoin('extensions', id, 'extension.js') : false;
                        } catch(err) { return false };
                    }(),
                    server: function() {
                        try {
                            var stats = fs.lstatSync(path.join(value, 'extension-server.js'));
                            return ( stats.isFile() ) ? path.join(value, 'extension-server.js') : false;
                        } catch(err) { return false };
                    }(),
                };
            }));
        });

        return d.promise;
    }
}