var hue = require('node-hue-api'),
    path = require('path');
var logger = require(path.join(__dirname, 'logger.js'))('API'),
    extensions = require(path.join(__dirname, 'extensions.js'));
var HueApi = hue.HueApi;

module.exports = function() {
    var options = require(path.join(__dirname, '../../options.json'));

    // Find bridges and start server + extensions
    logger.log('Searching for Hue bridges...');
    hue.nupnpSearch().then(function(bridges) {
        logger.log('Found %s %s', bridges.length, ( bridges.length > 1 ) ? 'bridges' : 'bridge');
        logger.log('Connecting to bridge %s with username \'%s\'', bridges[0].ipaddress, options.username);

        // Start API
        var api = require(path.join(__dirname, 'api.js'));
        api.create(new HueApi(bridges[0].ipaddress, options.username));

        // Run server-side extensions
        extensions.start();
    }).done();
}();