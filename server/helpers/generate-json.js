var fs = require('fs'),
    path = require('path');
var logger = require(path.join(__dirname, 'logger.js'))('SETUP');

module.exports = function() {
    var dest = path.join(__dirname, '../../options.json');
    var options = {
        username: '',
        server: {
            port: 384
        }
    };

    try {
        fs.lstatSync(dest);
    } catch(e) {
        logger.log('Generating global options file (%s)...', dest);

        try {
            fs.writeFileSync(dest, JSON.stringify(options, null, 4), 'utf-8');
        } catch(e) {
            logger.error('Could not generate global options file (%s), please re-run npm install again after fixing this issue:\n', dest, e);
        }

        return;
    }

    logger.log('Global options file exists already, skipped creation');
}();