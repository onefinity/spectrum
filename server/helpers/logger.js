var fs = require('fs'),
    path = require('path'),
    util = require('util'),
    moment = require('moment');
var logFile = fs.createWriteStream(path.join(__dirname, '../../spectrum.log'), { flags : 'a' });

var _log = function(args) {
    var line = new moment().format('[[]HH:mm:ss[]] ') + util.format.apply(this, args);

    // Log to console
    console.log(line);

    // Log to file
    logFile.write(line + '\n');
}

module.exports = function(type) {
    return {
        log: function() {
            var args = Array.prototype.slice.call(arguments);
            args[0] = '[' + type + '] ' + args[0];

            _log(args);
        },
        error: function() {
            var args = Array.prototype.slice.call(arguments);
            args[0] = '[' + type + '] ERROR: ' + args[0];

            _log(arguments);
        }
    }
}