var fs = require('fs'),
    path = require('path');
var logger = require(path.join(__dirname, 'helpers/logger.js'))('SETUP'),
    extensions = require(path.join(__dirname, 'helpers/extensions.js'));

module.exports = function() {
    // Check if this file exists and don't execute hue.nupnpSearch() unless a username is specified
    var optionsFile = path.join(__dirname, '../options.json');

    try {
        fs.lstatSync(optionsFile);
    } catch(err) {
        switch ( err.code ) {
            case 'ENOENT':
                logger.log('Spectrum\'s global options file (%s) does not exist and has to be generated', optionsFile);
                require(path.join(__dirname, 'helpers/generate-json.js'));
                break;
            default:
                logger.log('Spectrum\'s global options file (%s) exists but can not be read successfully...\n', optionsFile, err);
                break;
        }
    }

    var options = require(optionsFile);
    if ( !options.username ) {
        // Setup a simple server that will handle the username when it's provided
        require(path.join(__dirname, 'helpers/setup.js'));
    } else {
        // A username was provided so we can start the main server
        require(path.join(__dirname, 'helpers/start.js'));
    }
}();